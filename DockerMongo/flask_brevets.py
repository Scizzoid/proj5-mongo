"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request, redirect, url_for, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import os
from pymongo import MongoClient
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

controls = {}

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('dist', type=float)
    date = request.args.get('date')
    time = request.args.get('time')
    control_num = request.args.get('control_num')
    date_time = date + time
    date_time = arrow.get(date_time, 'YYYY-MM-DDHH:mm', tzinfo='utc').isoformat()
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, dist, date_time)
    close_time = acp_times.close_time(km, dist, date_time)

    # Add the control values to the dictionary of current controls
    controls[control_num] = [km, open_time, close_time]

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############
@app.route("/_submit_times")
def _submit_times():
    """
    Upload the current inputted brevet control lengths
    and their corresponding opening and closing times
    to the database.
    """
    # If controls is empty
    if (not controls):
        # Nothing submitted
        result = {"submitted": False}
        return flask.jsonify(result=result)

    else:
        for key in controls:
            item = { 'km': controls[key][0],
                     'open_time': controls[key][1],
                     'close_time': controls[key][2]
                     }
            db.tododb.insert_one(item)
        # Submissions have been inserted.
        result = {"submitted": True}
    
    return flask.jsonify(result=result)


@app.route("/_show_times")
def _show_times():
    """
    Redirects the user to a page containing all inputted
    control lengths, with their corresponding opening
    and closing times.
    """
    _items = db.tododb.find()
    items = [item for item in _items]

    # Switch ISO 8601 date format for same format shown on webpage
    for item in items:
        item["open_time"] = arrow.get(item["open_time"]).format("ddd M/D H:mm")
        item["close_time"] = arrow.get(item["close_time"]).format("ddd M/D H:mm")
    
    # Empty the controls dictionary now they have been added to the db
    controls.clear()

    return render_template('show_times.html', items=items)

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
